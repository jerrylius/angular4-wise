import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'confirm-dialog',
    // styleUrls: ['confirmDialog.component.scss'],
    templateUrl: './template/confirmation-dialog.component.html'
})

export class ConfirmationDialogComponent implements OnInit {
    public title: string;
    public message: string;
    public titleAlign?: string;
    public messageAlign?: string;
    public btnOkText?: string;
    public btnCancelText?: string;
    
    constructor(private MatDialogRef : MatDialogRef<ConfirmationDialogComponent>, @Inject(MAT_DIALOG_DATA) data: any){
        // this.config = data;
    }
    public ngOnInit() { }
}
