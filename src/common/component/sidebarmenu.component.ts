import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'side-bar-menu',
    template: `
    <div class="page-sidebar navbar-collapse collapse" style="position: fixed; z-index:5; top: 46px; height: 100% !important;">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <sidebar-menu-item [menuData]="menuData" class="page-sidebar-menu" first-level="true" ng-class="{'page-sidebar-menu-closed': settings.layout.pageSidebarClosed}"></sidebar-menu-item>
     </div>
    `
})

export class SideBarMenu implements OnInit {
    public ngOnInit() {

    }
}


@Component({
    selector: 'side-bar-item',
    templateUrl:'./template/sidebarMenuItem.html'
})

export class SideBarMenuItem implements OnInit {
    public ngOnInit() {

    }
}
