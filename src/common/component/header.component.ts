import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, Renderer2 } from '@angular/core';
import { UserService } from 'common/service/user.service';
import { Session } from 'common/service/session.service';
import { CompanyAndFacilityHeaderService } from 'common/service/companyAndFacilityDisplay.service';
import { ModuleConfig } from 'common/module-config'
import { Router, NavigationStart } from '@angular/router';
import *as _ from 'lodash';
@Component({
    selector: 'header',
    templateUrl: './template/header.component.html',
    providers: [
        ModuleConfig
    ],
})

export class HeaderComponent implements OnInit {

    @ViewChild('companyfacilityMenu')
    companyfacilityDiv: ElementRef;
    @ViewChild('userMenu')
    userMenuDiv: ElementRef;
    constructor(
        public userService: UserService,
        public session: Session,
        public router: Router,
        public moduleConfig: ModuleConfig,
        public companyAndFacilityDisplayService: CompanyAndFacilityHeaderService,
        public elementRef: ElementRef,
        private renderer: Renderer2
    ) {

        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                this.setCompanyAndFacilityDisplay;
            }
            // NavigationEnd
            // NavigationCancel
            // NavigationError
            // RoutesRecognized
        })
    }
    public locationPath;
    public modules;
    public user = { assignedCompanyFacilities: [{}] };
    public companyFacilities;
    public currentCompanyFacility;
    public cfShowLabel;

    ngAfterViewInit() { // 模板中的元素已创建完成

        // let greetDiv: HTMLElement = this.elementRef.nativeElement.querySelector('div'); 
        // greetDiv.style.backgroundColor = 'red';

        // this.renderer.listen(this.companyfacilityDiv.nativeElement, 'mouseover', (e) => {
        //     this.renderer.addClass(this.companyfacilityDiv.nativeElement.querySelector('.dropdown-user'), "open");
        // });
        // this.renderer.listen(this.companyfacilityDiv.nativeElement, 'mouseout', (e) => {
        //     this.renderer.removeClass(this.companyfacilityDiv.nativeElement.querySelector('.dropdown-user'), "open");
        // })
        // this.renderer.listen(this.userMenuDiv.nativeElement, 'mouseover', (e) => {
        //     this.renderer.addClass(this.userMenuDiv.nativeElement.querySelector('.dropdown-user'), "open");
        // });
        // this.renderer.listen(this.userMenuDiv.nativeElement, 'mouseout', (e) => {
        //     this.renderer.removeClass(this.userMenuDiv.nativeElement.querySelector('.dropdown-user'), "open");
        // })
        this.onMouseOver(this.companyfacilityDiv.nativeElement);
        this.onMouseOver(this.userMenuDiv.nativeElement);
        this.onMouseOut(this.companyfacilityDiv.nativeElement);
        this.onMouseOut(this.userMenuDiv.nativeElement);
    }

    onMouseOver(element) {
        this.renderer.listen(element, 'mouseover', (e) => {
            this.renderer.addClass(element.querySelector('.dropdown-user'), "open");
        });
    }

    onMouseOut(element) {
        this.renderer.listen(element, 'mouseout', (e) => {
            this.renderer.removeClass(element.querySelector('.dropdown-user'), "open");
        });
    }

    public ngOnInit() {
        const cxt = this;

        this.locationPath = location.hash;
        let moduleConfig = this.moduleConfig.getModuleMenu();
        this.userService.getUserInfo().then(function (userInfo) {
            cxt.userService.setUserCompanyFacilityObjsInfoById(userInfo.defaultCompanyFacility,
                userInfo.assignedCompanyFacilities, function () {
                    cxt.user = userInfo;
                    var userRoles = _.map(userInfo.roles, 'name');
                    cxt.modules = _.filter(moduleConfig, function (module: any) {
                        return module.label && (!module.roles || _.difference(module.roles, userRoles).length < _.uniqBy(module.roles, userRoles).length);
                    });
                    cxt.setCompanyAndFacilityDisplay(null, cxt.router);
                });
        });
    }
    public signOut() {
        if (this.session.getSsoMark() == "sso") {
            this.session.clean();
            location.href = linc.config.ssoRedirectLink + "?slo=true";
            return;
        }

        this.userService.signOut();
        this.router.navigate(['login']);
    };

    public isHomePage() {
        if (this.router.url == "home") return true;
        return false;
    };

    public isShowMenuBtn() {
        if (this.router.url == "home") return false;
        if (this.router.url == "system.feedback") return false;
        if (this.router.url == "gis.resources") return false;
        if (this.router.url == "gis.setup") return false;

        var mark = this.session.getMenuMark();
        if (mark != "hide") return false;

        return true;
    };

    public isShowCloseMenuBtn() {
        if (this.router.url == "home") return false;
        if (this.router.url == "system.feedback") return false;
        if (this.router.url == "gis.resources") return false;
        if (this.router.url == "gis.setup") return false;

        if (this.locationPath != location.hash) {
            this.session.setMenuMark("show");
            this.locationPath = location.hash;
            return true;
        }

        var mark = this.session.getMenuMark();
        if (mark == "hide") return false;

        return true;
    };

    public showMenu() {
        this.session.setMenuMark("show");
    };

    public hideMenu() {
        this.session.setMenuMark("hide");
    };


    public menuName() {
        var ctx = this;
        var module = _.find(this.modules, function (module) {
            return module.entryUrl == ctx.router.url
                || ctx.router.url.indexOf(module.entryUrl) == 0;
        });
        if (module) {
            return module.label;
        }
        return "Home Menu";
    };


    private setCompanyAndFacilityDisplay(event, toState) {
        if (!this.user) return;
        var cfShowLabel = this.getCfShowLabel(toState);
        this.cfShowLabel = cfShowLabel;
        if (!cfShowLabel) return;
        this.currentCompanyFacility = this.session.getCompanyFacility();
        if (cfShowLabel == "companyfacility") {
            this.companyFacilities = this.user.assignedCompanyFacilities;
        } else {
            this.companyFacilities = this.getCfSelectOptions(cfShowLabel);

            this.setCurrentCfIntoOptionsByCfShowLabel(cfShowLabel);
        }
    }

    private setCurrentCfIntoOptionsByCfShowLabel(cfShowLabel) {
        _.forEach(this.companyFacilities, function (cf, key) {
            if (cf[cfShowLabel].id == this.currentCompanyFacility[cfShowLabel].id) {
                this.companyFacilities[key] = this.currentCompanyFacility;
            }
        });
    }

    private getCfSelectOptions(type) {
        var companyFacilities = this.user.assignedCompanyFacilities;
        return _.uniqWith(companyFacilities, function (cf1, cf2) {
            return (cf1[type].id == cf2[type].id)
        });
    }

    private getCfShowLabel(toState) {
        var displayConfig = this.companyAndFacilityDisplayService.getDisplayByStateName(toState.url);
        if (displayConfig.indexOf("facility") > -1 && displayConfig.indexOf("company") > -1) {
            return "companyfacility";
        } else if (displayConfig.indexOf("company") > -1) {
            return "company";
        } else if (displayConfig.indexOf("facility") > -1) {
            return "facility";
        } else {
            null;
        }
    }

    private isChangeCurrentCompanyFacility(cf, currentCf, cfShowLabel) {
        var bool;
        if (cfShowLabel == "company") {
            bool = (cf.companyId == currentCf.companyId) ? false : true;
        } else if (cfShowLabel == "facility") {
            bool = (cf.facilityId == currentCf.facilityId) ? false : true
        } else if (cfShowLabel == "companyfacility") {
            bool = (cf.companyId == currentCf.companyId &&
                cf.facilityId == currentCf.facilityId) ? false : true;
        }
        return bool;
    }

    public onClickCompanyFacility(cf) {
        if (this.isChangeCurrentCompanyFacility(cf, this.currentCompanyFacility, this.cfShowLabel)) {
            this.session.setCompanyFacility(cf);
            this.currentCompanyFacility = cf;
            var current = this.router.url;
            //gis切换仓库不跳转页面
            if (!_.startsWith(current, "gis")) {
                this.router.navigate(['home']);
            }
        }
    };

    // Handles the horizontal menu
    public handleHorizontalMenu() {


        // console.log(this.elementRef.nativeElement)
        // this.elementRef.nativeElement.querySelector('.top-menu').on('click', '.dropdown-user', function (e) {
        //     var currentEl = this.nativeElement;
        //     currentEl.addClass("open");

        // });
        // this.elementRef.nativeElement.querySelector('.top-menu').on('mouseover', '.dropdown-user', function (e) {
        //     var currentEl = this.nativeElement;
        //     currentEl.addClass("open");

        // });

        // this.elementRef.nativeElement.querySelector('.top-menu').on('mouseout', '.dropdown-user', function (e) {
        //     var currentEl = this.nativeElement;
        //     currentEl.removeClass("open");
        //     currentEl.unbind("blur");

        // });


        // //handle tab click
        // this.elementRef.nativeElement.querySelector('.page-header-inner').on('click', '.hor-menu a[data-toggle="tab"]', function (e) {
        //     e.preventDefault();
        //     var nav = e.querySelector(".hor-menu .nav");
        //     var active_link = nav.querySelector('li.current');
        //     this.elementRef.nativeElement.querySelector('li.active', active_link).removeClass("active");
        //     this.elementRef.nativeElement.querySelector('.selected', active_link).remove();
        //     var new_link = e.parents('li').last();
        //     new_link.addClass("current");
        //     new_link.querySelector("a:first").append('<span class="selected"></span>');
        // });

        // // handle search box expand/collapse        
        // this.elementRef.nativeElement.querySelector('.page-header-inner').on('click', '.search-form', function (e) {
        //     var currentEl = this.nativeElement;
        //     currentEl.addClass("open");
        //     currentEl.querySelector('.form-control').focus();

        //     this.elementRef.nativeElement.querySelector('.page-header-inner .search-form .form-control').on('blur', function (e) {
        //         currentEl.closest('.search-form').removeClass("open");
        //         currentEl.unbind("blur");
        //     });
        // });

        // // handle hor menu search form on enter press
        // this.elementRef.nativeElement.querySelector('.page-header-inner').on('keypress', '.hor-menu .search-form .form-control', function (e) {
        //     if (e.which == 13) {
        //         this.nativeElement.closest('.search-form').submit();
        //         return false;
        //     }
        // });

        // // handle header search button click
        // this.elementRef.nativeElement.querySelector('.page-header-inner').on('mousedown', '.search-form.open .submit', function (e) {
        //     e.preventDefault();
        //     e.stopPropagation();
        //     e.closest('.search-form').submit();
        // });

        // // handle hover dropdown menu for desktop devices only
        // this.elementRef.nativeElement.querySelector('[data-hover="megamenu-dropdown"]').not('.hover-initialized').each(function (e) {
        //     this.nativeElement.dropdownHover();
        //     this.nativeElement.addClass('hover-initialized');
        // });

        // this.elementRef.nativeElement.on('click', '.mega-menu-dropdown .dropdown-menu', function (e) {
        //     e.stopPropagation();
        // });
    };

}

