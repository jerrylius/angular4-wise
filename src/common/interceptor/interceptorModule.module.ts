import { NgModule } from "@angular/core";
import {HttpClientModule , HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpsRequestInterceptor } from 'common/interceptor/httpsRequestInterceptor.service';

@NgModule({
    imports: [HttpClientModule],
    providers: [{
        provide: HTTP_INTERCEPTORS,
        useClass: HttpsRequestInterceptor,
        multi: true,
    }]
})

export class InterceptorModule { }