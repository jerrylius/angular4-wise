
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Session } from 'common/service/session.service';
import { Router } from '@angular/router';
import 'rxjs/add/operator/do';
export type objecType = {
    [key: string]: any
};

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
    constructor(private session: Session, private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        // req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        const setReq = this.setRuleUrl(req);
        if (setReq.body) {
            setReq.body = JSON.stringify(setReq.body);
        }
        const headers = this.setHeaderInformation(setReq);
        headers['Content-Type'] = 'application/json';
        let reqOptions = {
            headers: new HttpHeaders(headers)
        }
        const newReq = req.clone(reqOptions);
        // const newReq = req.clone({ setHeaders: headers });

        return next.handle(newReq).do(event => {
       
            console.log(event);
            if (event instanceof HttpResponse) {
                if (event.status == 401) {

                    this.session.clean();

                    if (location.hash.indexOf("/sso#") < 0) {
                        this.router.navigate(['login']);
                    }
                }

            }
        });
    }

    setHeaderInformation(req) {

        const headers: objecType = {};

        if (req.url.indexOf("login") < 0) {
            if (this.session.getUserToken())
                headers.Authorization = this.session.getUserToken();
        }
        const sessionCompanyFacility = this.session.getCompanyFacility();
        if (!req.headers["WISE-Company-Id"]) {
            if (req.data && req.data.wiseCompanyId) {
                headers["WISE-Company-Id"] = req.data.wiseCompanyId;
            }
            // else if (sessionCompanyFacility&ifAddCompanyToHeader()) {
            //     headers["WISE-Company-Id"] = sessionCompanyFacility.companyId;
            // } 
            else if (sessionCompanyFacility) {
                headers["WISE-Company-Id"] = this.session.getCompanyIdsByFacilityId(sessionCompanyFacility.facilityId).join(",");
            }
        }
        if (sessionCompanyFacility) {
            headers["WISE-Facility-Id"] = sessionCompanyFacility.facilityId;
        }

        return headers;
    }

    setRuleUrl(req) {
        const sessionCompanyFacility = this.session.getCompanyFacility();
        if (req.url.startsWith("/fd-app/") || req.url.startsWith("/idm-app/") ||
            req.url.startsWith("/print-app/") || req.url.startsWith("/file-app/") || req.url.startsWith("/push-app/")) {
            req.url = "/shared" + req.url;
            if (process.env.NODE_ENV === "prod") {
                req.url = "/v2" + req.url;
            }
        } else if (req.url.startsWith("/bam/")
            || req.url.startsWith("/base-app/")
            || req.url.startsWith("/wms-app/")
            || req.url.startsWith("/yms-app/")
            || req.url.startsWith("/inventory-app/")
        ) {
            if (sessionCompanyFacility) {
                req.url = "/" + sessionCompanyFacility.facility.accessUrl + req.url;
                if (process.env.NODE_ENV === "prod") {
                    req.url = "/v2" + req.url;
                }
            }
        } else if (req.url.endsWith(".json") && (req.url.indexOf("/data/") > -1)) {
            if (process.env.NODE_ENV === "prod") {
                req.url = req.url.replace(/data/, "v2/data");
            }
        }
        return req;
    }


    // ifAddCompanyToHeader() {
    //     var companyAndFacilityDisplayService = $injector.get('companyAndFacilityDisplayService');
    //     var $state = $injector.get('$state');
    //     var displayConfig = companyAndFacilityDisplayService.getDisplayByStateName($state.current.name);
    //     if (displayConfig.indexOf("company") > -1) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}



// @Injectable()
// export class AuthInterceptor implements HttpInterceptor {
//   constructor(private auth: AuthService) {}

//   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//     // Get the auth header from the service.
//     const authHeader = this.auth.getAuthorizationHeader();
//     // Clone the request to add the new header.
//     const authReq = req.clone({headers: req.headers.set('Authorization', authHeader)});
//     // Pass on the cloned request instead of the original request.
//     return next.handle(authReq);
//   }
// }