import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class CompanyService {
    constructor(
        public http: HttpClient
    ) { }
    
   
    public getCompanyByOrgId(orgId: string) {
        return this.http.get("/fd-app/company/" + orgId).toPromise();
    };

    public searchCompany(params: any) {
        return this.http.post("/fd-app/company/search",params).toPromise();
    };

    public createAndUpdateCompany(orgId: string, company: any) {

        this.http.put("/fd-app/company/" + orgId, company).toPromise();
    };

    public deleteFacility(orgId:string){
        return this.http.delete("/fd-app/company/" + orgId).toPromise();
    };
}