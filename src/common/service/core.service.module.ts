/* tslint:disable:member-ordering no-unused-variable */
import {
  ModuleWithProviders, NgModule,
  Optional, SkipSelf
} from '@angular/core';

import { CommonModule } from '@angular/common';

import { Session } from './session.service';
import { FacilityService } from './facility.service';
import { CompanyService } from './company.service';
import { UserService } from './user.service';
import { LincUtilService } from './lincUtil.service';
import { CompanyAndFacilityHeaderService } from './companyAndFacilityDisplay.service';

@NgModule({
  imports: [CommonModule],
  declarations: [],
  exports: [],
  providers: [Session, FacilityService, CompanyService, UserService, CompanyAndFacilityHeaderService, LincUtilService]
})
export class CoreServiceModule {

  constructor( @Optional() @SkipSelf() parentModule: CoreServiceModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

}
