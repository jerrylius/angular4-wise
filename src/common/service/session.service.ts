import { Injectable } from '@angular/core';
import * as _ from 'lodash';
export type sessionType = {
    [key: string]: any
};

@Injectable()
export class Session {

    public _sessionInfo: sessionType = {};
    /**
     * Already return a clone of the current sessionInfo.
     */
    public get sessionInfo() {
        return this._sessionInfo = this._clone(this._sessionInfo);
    }
    /**
     * Never allow mutation
     */
    public set sessionInfo(value) {
        throw new Error('do not mutate the `.sessionInfo` directly');
    }

    public get(prop?: any) {
        /**
         * Use our sessionInfo getter for the clone.
         */
        const sessionInfo = this.sessionInfo;
        return sessionInfo.hasOwnProperty(prop) ? sessionInfo[prop] : sessionInfo;
    }


    public setAssignedCompanyFacilities = function (assignedCompanyFacilities) {
        this.setSessionData("assignedCompanyFacilities", assignedCompanyFacilities);
        if (Storage !== null) {
            this.setToStorage("assignedCompanyFacilities", assignedCompanyFacilities, true);
        }
    };
    public setUserInfo = function (userInfo: any) {
        this.setSessionData("userInfo", userInfo);
    };

    public setUserPermission = function (userPermissions: any) {
        this.setSessionData("userPermissions", userPermissions);
        if (Storage !== null) {
            this.setToStorage("userPermissions", userPermissions, true);
        }
    };
    public setUserId = function (userId: string) {
        this.setSessionData("userId", userId);
        if (Storage !== null) {
            this.setToStorage("userId", userId, false);
        }
    };

    public setUserToken(token: string) {
        this.setSessionData("token", token);
        if (Storage !== null) {
            this.setToStorage("token", token, false);
        }
    }


    public setCompanyFacility(companyFacility: string) {
        this.setSessionData("companyFacility", companyFacility);
        if (Storage !== null) {
            this.setToStorage("companyFacility", companyFacility, true);
        }
    };

    public setMenuMark(mark) {
        this.setSessionData("menuMark", mark);
        if (Storage !== null) {
            this.setToStorage("menuMark", mark, false);
        }
    };

    public setSsoMark() {
        this.setSessionData("ssoMark", "sso");
        if (Storage !== null) {
            this.setToStorage("ssoMark", "sso", false);
        }
    };

    public clean() {
        this._sessionInfo = {};
        if (Storage !== null) {
            localStorage.clear();
        }
    };


    public getSsoMark() {
        return this.getSessionData("ssoMark") || this.getFromStorageAndSetToSessionDataIfExist("ssoMark", false)
    };

    public getMenuMark() {
        return this.getSessionData("menuMark") || this.getFromStorageAndSetToSessionDataIfExist("menuMark", false)
    };

    public getUserId() {
        return this.getSessionData("userId") || this.getFromStorageAndSetToSessionDataIfExist("userId", false);
    };

    public getUserToken() {
        return this.getSessionData("to)ken") || this.getFromStorageAndSetToSessionDataIfExist("token", false)
    };

    public getCompanyFacility() {
        return this.getSessionData("companyFacility") || this.getFromStorageAndSetToSessionDataIfExist("companyFacility", true)
    };

    public getCompanyIdsByFacilityId(facilityId: string) {
        var companyIds = [];
        _.forEach(this.getAssignedCompanyFacilities(), function (companyFacility) {
            if (companyFacility.facilityId == facilityId) {
                companyIds.push(companyFacility.companyId);
            }
        });
        return companyIds;
    };

    getAssignedCompanyFacilities() {
        return this.getSessionData("assignedCompanyFacilities") || this.getFromStorageAndSetToSessionDataIfExist("assignedCompanyFacilities", true)
    };



    private setSessionData(key: string, value: any) {
        this._sessionInfo[key] = value;
    }

    private setToStorage(key: string, value: string, storedAsObject: boolean) {
        if (storedAsObject)
            this.setItemToStorage(key, value);
        else
            localStorage[key] = value;
    }

    private setItemToStorage(key: string, value: string) {
        if (value) {
            localStorage.setItem(key, JSON.stringify(value));
        }
    }

    // private set(prop: string, value: any) {
    //     /**
    //      * Internally mutate our sessionInfo.
    //      */
    //     return this._sessionInfo[prop] = value;
    // }

    private getSessionData(key: string) {
        return this._sessionInfo[key];
    }


    private getFromStorageAndSetToSessionDataIfExist(key: string, storedAsObject: boolean) {
        if (storedAsObject) {
            if (localStorage.getItem(key) !== null) {
                this.setSessionData(key, this.getItemFromStorage(key));
                return this.getSessionData(key)
            } else {
                return null;
            }
        } else {
            if (localStorage[key]) {
                this.setSessionData(key, localStorage[key]);
                return this.getSessionData(key);
            } else {
                return null;
            }
        }
    }


    private getItemFromStorage(key: string) {
        var item = localStorage.getItem(key);

        if (item !== null && typeof item === 'string') {
            var itemValue = null;
            try {
                itemValue = JSON.parse(item);
            } catch (err) {
            }
            return itemValue;
        } else {
            return item;
        }
    }

    private _clone(object: sessionType) {
        /**
         * Simple object clone.
         */
        return JSON.parse(JSON.stringify(object));
    }
}
