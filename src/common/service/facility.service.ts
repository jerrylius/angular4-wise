import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class FacilityService {
    constructor(
        public http: HttpClient
    ) { }
  

    public getFacilityByOrgId(orgId: string) {
        return this.http.get("/fd-app/facility/" + orgId).toPromise();

    };

    public searchFacility(params: any) {
        return this.http.post("/fd-app/facility/search", params).toPromise();
        // return $resource("/fd-app/facility/search", null, resourceConfig).search(params).$promise;
    };

    public createAndUpdateFacility(orgId: string, facility: any) {

        this.http.put("/fd-app/facility/" + orgId, facility).toPromise();
        // return $resource("/fd-app/facility/:id", {id: orgId}, resourceConfig).update(facility).$promise;
    };

    public deleteFacility(orgId: string) {
        return this.http.delete("/fd-app/facility/" + orgId).toPromise();
        // return $resource("/fd-app/facility/:id", {id: orgId}).delete().$promise;
    };

}