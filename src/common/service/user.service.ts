import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FacilityService } from 'common/service/facility.service';
import { CompanyService } from 'common/service/company.service';
import { LincUtilService } from 'common/service/lincUtil.service';
import { Session } from 'common/service/session.service'
import * as _ from 'lodash';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import { promise } from 'selenium-webdriver';

@Injectable()
export class UserService {

    constructor(
        public http: HttpClient,
        public facilityService: FacilityService,
        public companyService: CompanyService,
        public lincUtilService: LincUtilService,
        public session: Session
    ) { }

    public Sign(user: object): Promise<any> {
        const url = '/shared/idm-app/user/login';
        return this.http.post(url, user).toPromise();
        // return this.http.post(url, JSON.stringify(user))
        //     .toPromise().then((response: Response) => response.json());
        // // .catch(this.handleError);
    }

    public searchUsers(param) {
        if (param) {
            param.facilityId = this.getFacilityId();
        }
        return this.http.post('/idm-app/user/search', param).toPromise();
    }

    // public getUserDetailById(userId: string): Promise<any> {
    //     const ctx = this;
    //     this.getUserById(userId).then(function (res) {
    //         ctx.setUserCompanyFacilityObjsInfoById(res.defaultCompanyFacility,
    //             res.assignedCompanyFacilities, function () {
    //                 return Promise.resolve(res);
    //             });
    //     }, function (error) {
    //         return Promise.reject(error);
    //     });
   

    // };

    public setUserCompanyFacilityObjsInfoById(defaultCf, assignedCfs, cbFun: Function) {
        let companyIds = _.uniq(_.map(assignedCfs, "companyId"));
        let facililtyIds = _.uniq(_.map(assignedCfs, "facilityId"));
        let observableBatch = [];
        observableBatch.push(this.facilityService.searchFacility({ ids: facililtyIds }));
        observableBatch.push(this.companyService.searchCompany({ ids: companyIds }));

        const cxt = this;
        Observable.forkJoin(observableBatch).subscribe(res => {

            let orgs = _.concat(res[0], res[1]);
            cxt.lincUtilService.extractOrganaizationName(orgs);
            let orgsMap = _.keyBy(orgs, 'id');
            _.forEach(assignedCfs, function (cf) {
                cxt.setCompanyFacilityObjInfoById(cf, orgsMap);
            });
            cxt.setCompanyFacilityObjInfoById(defaultCf, orgsMap);
            cbFun();
            // for (let item of res) {
            //     //需要遍历请求数组结果集，item便是一个请求返回的结果集

            // }
        });

    };


    public getUserInfo(): Promise<any> {
        if (this.session._sessionInfo.userInfo) {
            return Promise.resolve(this.session._sessionInfo.userInfo);
        } else if (this.session.getUserId() !== undefined) {

            return this.getUserById(this.session.getUserId());
        } else {
            return Promise.resolve(null);
        }
    }

    private setCompanyFacilityObjInfoById(cf, orgsMap) {
        if (!cf) return;
        if (cf.companyId) {
            cf.company = orgsMap[cf.companyId];
        }
        if (cf.facilityId) {
            cf.facility = orgsMap[cf.facilityId];
        }
    }

    public getUserById(userId: string): Promise<any> {
        return this.http.get('/shared/idm-app/user/' + userId).toPromise();
    };


    private getFacilityId() {
        const facilityId = "";
        if (location.hash.indexOf("user-management") > 0) {
            return facilityId;
        }
        return facilityId;
    }

    public getDefaultCompanyFacility(cf, assignedCfs) {
        var defaultCf = cf;
        if (assignedCfs && assignedCfs.length > 0) {
            if (!defaultCf) {
                defaultCf = assignedCfs[0];
            }
        }
        return defaultCf;
    };

    public signOut(): Promise<any> {
        return this.http.post("/idm-app/user/logout", { oauthToken: this.session.getUserToken()}).toPromise().then( (res)=> {
            this.session.clean();
        },(error)=> {
            this.session.clean();
        });
    };

    public isSignIn() {
        return this.session.getUserId();
    };

    //  Sign(user?: LoginParams): Promise<LoginEntitny> {
    //     const url = '/shared/idm-app/user/login';
    //     return this.http.post(url, JSON.stringify(user), { headers: this.headers })
    //         .toPromise()
    //         .then(response => response.json())
    //         .catch(this.handleError);
    // }
    // private handleError(error: any): Promise<any> {
    //     console.error('An error occurred', error); // for demo purposes only
    //     return Promise.reject(error.message || error);
    // }
    // private handleError(error: any): Promise<any> {
    //     console.error('An error occurred', error); // for demo purposes only
    //     return Promise.reject(error.message || error);
    // }

}



    //     let post1 = this.http.get(`${this.apiUrl}/1`);
    //     let post2 = this.http.get(`${this.apiUrl}/2`);

    //     Observable.forkJoin([post1, post2])
    //      .subscribe(results => {
    //       this.post1 = results[0];
    //       this.post2 = results[1];
    //      });
    //    }

// export class LoginParams {
//     returnUserPermissions: Array<string> = ["WEB"];
//     username: string = 'quinnc';
//     password: string = 'uiop7890';
// }

// export class LoginEntity {
//     idmUserId: string;
//     oAuthToken: string;
//     success: string;
//     userPermissions: Array<UserPermissionsEntity>;
// }

// // /**
// //  * 
// //  */
// export class UserPermissionsEntity {
//     name: string;
//     category: string;
// }