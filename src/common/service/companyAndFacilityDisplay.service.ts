import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import *as _ from 'lodash';

@Injectable()
export class CompanyAndFacilityHeaderService {


    public displayConfigs=[
        {"state": "user", "display": []},
        {"state": "fd", "display": ["company"]},
        {"state": "cf.company", "display": ["company"]},
        {"state": "cf.facility", "display": ["facility"]},
        {"state": "cf.facility.facility-management", "display": []},
        {"state": "cf.company.company-management", "display": []},
        {"state": "gis.resources", "display": ["facility"]},
        {"state": "gis.setup", "display": ["facility"]}
    ];
    constructor(
        public http: Http
    ) {
        // this.http.get('../data/header/company_and_facility_header_display.json').toPromise().then(function (response) {
        //     this.displayConfigs = response;
        // }); 
    }

   
    public getDisplayByStateName(stateName) {

        var displayConfig = _.findLast(this.displayConfigs, function (config) {
            return stateName.indexOf(config.state) > -1;

        });
        if (displayConfig) {
            return displayConfig.display;
        } else {
            return ["facility", "company"];
        }


    };

    public getDefaultCompanyFacility(cf, assignedCfs) {
        var defaultCf = cf;
        if (assignedCfs && assignedCfs.length > 0) {
            if (!defaultCf) {
                defaultCf = assignedCfs[0];
            }
        }
        return defaultCf;
    };
}


