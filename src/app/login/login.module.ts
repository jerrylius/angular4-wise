import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { LoginRoute } from './login.routing';
import { LoginComponent } from './login.component';

@NgModule({
    imports: [
        FormsModule,
        LoginRoute
    ],
    declarations: [
        LoginComponent
    ],
    providers: []
})

export class LoginModule { }