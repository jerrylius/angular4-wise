import {
    Component,
    OnInit
} from '@angular/core';

import * as _ from 'lodash';
import { UserService } from 'common/service/user.service';
import { Session } from 'common/service/session.service';
import { Router } from '@angular/router';

@Component({
    selector:'login',
    templateUrl: './template/login.html'
})

export class LoginComponent {
    private loginUser: LoginParams = new LoginParams();
    // private loginUser:{};
    // public loginEntity: Array<LoginEntity>; 

    constructor(
        private userService: UserService,
        private router: Router,
        public session: Session
    ) { }

    onClickSubmit() {
        const cxt = this;
        this.userService.Sign(this.loginUser).then((res) => {
            this.session.setUserToken(res.oAuthToken);
            this.session.setUserId(res.idmUserId);
            this.session.setUserPermission(_.map(res.userPermissions, 'name'));
            this.setUserAndCompanyFacilityToSession(res.idmUserId, cxt, function () {
                cxt.router.navigate(['home']);
            });
        }, function (err) {
            console.log(err);
            console.log(err.error)
        });

    }

    setUserAndCompanyFacilityToSession(userId: string, cxt, cbFun: Function) {

        cxt.userService.getUserById(userId).then(function (res) {
            cxt.userService.setUserCompanyFacilityObjsInfoById(res.defaultCompanyFacility, res.assignedCompanyFacilities, function () {
                cxt.session.setUserInfo(res);
                cxt.session.setAssignedCompanyFacilities(res.assignedCompanyFacilities);
                const defaultCf = cxt.userService.getDefaultCompanyFacility(res.defaultCompanyFacility, res.assignedCompanyFacilities);
                cxt.session.setCompanyFacility(defaultCf);
                cbFun();
            })


        }, function (err) {
            // lincUtil.errorPopup("Internal Server Error, please contact IT.");
        });
    }

    value = '';
    onEnter(value: string) {

    }
}

export class LoginParams {
    returnUserPermissions: Array<string> = ["WEB"];
    username: string = 'quinnc';
    password: string = 'uiop7890';
}