import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { NoContentComponent } from './no-content';
import { LoginComponent } from './login/login.component'

export const ROUTES: Routes = [
  // { path: 'home', component: HomeComponent},
  // { path: 'wms', component: HomeComponent},
  { path: 'wms', loadChildren: 'app/wms/wms.module#WmsModule' },
  { path: 'detail', loadChildren: './+detail#DetailModule' },
  { path: 'barrel', loadChildren: './+barrel#BarrelModule' },
  { path: '**', component: NoContentComponent },
];
