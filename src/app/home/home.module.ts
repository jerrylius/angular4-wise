import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { IndexComponent } from './index.component';
import { HomeRoutingModule } from './home.routing';
import { SharedModule } from '../../common/component/shared.module'

@NgModule({
    imports: [CommonModule, SharedModule, FormsModule, HomeRoutingModule],
    declarations: [IndexComponent],
    providers: []
})
export class HomeModule { }