import {
  Component,
  OnInit
} from '@angular/core';

import { UserService } from 'common/service/user.service';
import { ModuleConfig } from 'common/module-config'
import * as _ from 'lodash';
@Component({
  selector: 'home',  // <home></home>
  providers: [
    ModuleConfig
  ],
  templateUrl: './template/home.component.html'
})
export class HomeComponent implements OnInit {

  public modules = [];

  constructor(
    private userService: UserService,
    private moduleConfig: ModuleConfig
  ) { }

  ngOnInit() {
    let moduleConfig = this.moduleConfig.getModuleMenu();
    const cxt = this;
    this.userService.getUserInfo().then(function (userInfo) {
        cxt.userService.setUserCompanyFacilityObjsInfoById(userInfo.defaultCompanyFacility,
            userInfo.assignedCompanyFacilities, function () { 
                var userRoles = _.map(userInfo.roles, 'name');
                cxt.modules = _.filter(moduleConfig, function (module: any) {
                    return module.label && (!module.roles || _.difference(module.roles, userRoles).length < _.uniqBy(module.roles, userRoles).length);
                });
            });
    });
  }

  public signOut() {
    this.userService.signOut().then(function () {
      this.router.navigate(['/login']);
    });
  };

}
