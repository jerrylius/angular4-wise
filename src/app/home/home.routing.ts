import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { IndexComponent } from './index.component';
export const routes: Routes = [
  
    {
        path: 'home',
        component: IndexComponent,
        children: [
            {
                path: '',
                component:HomeComponent

            }
        ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }