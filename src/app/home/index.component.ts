import { Component } from '@angular/core';

@Component({
  template: `
    <header class="page-header navbar navbar-fixed-top"></header>
    <router-outlet></router-outlet>
    <footer class="footer"></footer>
  `
})
export class IndexComponent {

}