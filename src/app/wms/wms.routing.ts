import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WmsComponent } from './wms.component';
import { WmsMainComponent } from './wms.main.component';
const wmsRoutes: Routes = [
    {
        path: 'wms',
        component: WmsComponent,
        children: [
            {
                path: '',
                component: WmsMainComponent,
                // children: [
                //   {
                //     path: ':id',
                //     component: CrisisDetailComponent
                //   },
                //   {
                //     path: '',
                //     component: CrisisCenterHomeComponent
                //   }
                // ]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(wmsRoutes)],
    exports: [RouterModule]
})
export class WmsRoutingModule { }