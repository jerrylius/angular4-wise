import { NgModule } from '@angular/core';
// import { CrisisCenterHomeComponent } from './crisis-center-home.component';
// import { CrisisListComponent }       from './crisis-list.component';
// import { CrisisCenterComponent }     from './crisis-center.component';
// import { CrisisDetailComponent }     from './crisis-detail.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WmsRoutingModule } from './wms.routing';
import { SharedModule } from '../../common/component/shared.module'
import { WmsMainComponent } from './wms.main.component';
import { WmsComponent } from './wms.component'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        WmsRoutingModule
    ],
    declarations: [
        WmsComponent,
        WmsMainComponent
    ]

})
export class WmsModule { }